#!/bin/bash

echo "Begin ..."

source .env

#gunzip < ../../wp-db/db_dump/dump.sql.gz | docker-compose exec -T mariadb sh -c "exec mysql -u root -p$MYSQL_ROOT_PASSWORD $MYSQL_DATABASE"
docker-compose exec -T mariadb sh -c "exec mysql -u root -p$MYSQL_ROOT_PASSWORD $MYSQL_DATABASE" < ../../wp-db/db_dump/dump.sql

read -p "Press enter to continue ..."


