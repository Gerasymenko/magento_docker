@echo off

if exist "./wp-data/wp-config.php" (
	echo "WordPress config file found."
) else (
	echo "WordPress config file not found. Installing..."
	docker-compose exec --user www-data php wp core download
	docker-compose exec --user www-data php wp core config --dbhost=mariadb --dbname=wordpress --dbuser=wordpress --dbpass=wordpress
)


pause