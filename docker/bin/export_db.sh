#!/bin/bash

echo "Begin export database ..."

source .env


FILENAME_BACKUP=dump_`date '+%d.%m.%Y_%H%M'`.sql
FILENAME_LAST=dump.sql


rm -rf ../../wp-db/db_dump/*

docker-compose exec -T mariadb sh -c "exec mysqldump --default-character-set=utf8 -u root -p$MYSQL_ROOT_PASSWORD $MYSQL_DATABASE -e 2> null" | gzip > ../../wp-db/db_backup/$FILENAME_BACKUP.gz
#docker-compose exec mariadb sh -c "exec mysqldump --default-character-set=utf8 -u root -p$MYSQL_ROOT_PASSWORD $MYSQL_DATABASE -e 2> null" | gzip > ../../wp-db/db_dump/$FILENAME_LAST.gz
docker-compose exec -T mariadb sh -c "exec mysqldump --default-character-set=utf8 -u root -p$MYSQL_ROOT_PASSWORD $MYSQL_DATABASE -e 2> null" > ../../wp-db/db_dump/$FILENAME_LAST



read -p "Press enter to continue ..."
